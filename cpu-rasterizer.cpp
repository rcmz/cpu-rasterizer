#include "rcmz.hpp"

using namespace rcmz;

void rasterizeTriangles(Array2<u8vec3>& colorBuffer, Array2<float>& depthBuffer, Mesh& mesh, mat4 objectMat, mat4 viewMat, mat4 projectionMat) {
    List<vec4> vertices(mesh.vertices.size());

    for (uint i : range(vertices.size())) {
        vec4& v = vertices[i];
        v = projectionMat * viewMat * objectMat * vec4(mesh.vertices[i], 1);
        v.xyz /= v.w;
        v.xy = mapRange(v.xy, -1, +1, 0, colorBuffer.size-1);
    }

    for (uint i : range(0, vertices.size(), 3)) {
        if (vertices[i+0].w <= 0 || vertices[i+1].w <= 0 || vertices[i+2].w <= 0) continue;

        vec4 v0 = vertices[i+0];
        vec4 v1 = vertices[i+1];
        vec4 v2 = vertices[i+2];

        uvec2 bboxMin = clamp(min(v0, v1, v2).xy, 0, colorBuffer.size-1);
        uvec2 bboxMax = clamp(max(v0, v1, v2).xy+1, 0, colorBuffer.size-1);

        for (uvec2 p : range(bboxMin, bboxMax)) {
            vec2 fc = p + 0.5;

            auto side = [](vec2 v, vec2 e0, vec2 e1) -> int {
                return sign((v.x - e0.x) * (e1.y - e0.y) - (v.y - e0.y) * (e1.x - e0.x));
            };
            auto equalOrZero = [](int a, int b, int c) -> bool {
                if (!a && !b) return true;
                if (!b && !c) return true;
                if (!c && !a) return true;
                if (!a) return equal(b, c);
                if (!b) return equal(c, a);
                if (!c) return equal(a, b);
                return equal(a, b, c);
            };
            auto positiveOrZero = [](int a, int b, int c) -> bool {
                return a >= 0 && b >= 0 && c >= 0;
            };
            if (!positiveOrZero(
                side(fc, v0.xy, v1.xy),
                side(fc, v1.xy, v2.xy),
                side(fc, v2.xy, v0.xy)
            )) continue;

            vec3 bc;
            float det = ((v1.y - v2.y) * (v0.x - v2.x) + (v2.x - v1.x) * (v0.y - v2.y));
            bc[0] = ((v1.y - v2.y) * (fc.x - v2.x) + (v2.x - v1.x) * (fc.y - v2.y)) / det;
            bc[1] = ((v2.y - v0.y) * (fc.x - v2.x) + (v0.x - v2.x) * (fc.y - v2.y)) / det;
            bc[2] = 1 - bc[0] - bc[1];

            float depth = dot(bc, vec3(v0.z, v1.z, v2.z));
            if (depth < 0 || depth > 1 || depth >= depthBuffer[p]) continue;
            depthBuffer[p] = depth;

            float z0 = v0.w;
            float z1 = v1.w;
            float z2 = v2.w;
            float z = 1 / dot(1 / vec3(z0, z1, z2), bc);

            // vec3 position = mat3x3(mesh.vertices[i+0], mesh.vertices[i+1], mesh.vertices[i+2]) * bc;
            // position = vec3(viewMat * objectMat * vec4(position, 1));

            // vec3 normal = normalize(cross(mesh.vertices[i+1] - mesh.vertices[i+0], mesh.vertices[i+2] - mesh.vertices[i+0]));
            vec3 normal = normalize(mat3x3(mesh.normals[i+0], mesh.normals[i+1], mesh.normals[i+2]) * bc);
            normal = normalize(vec3(viewMat * objectMat * vec4(normal, 0)));

            // vec2 texCoord = mat2x3(mesh.texCoords[i+0], mesh.texCoords[i+1], mesh.texCoords[i+2]) * bc;
            vec2 texCoord = (mat2x3(mesh.texCoords[i+0] / z0, mesh.texCoords[i+1] / z1, mesh.texCoords[i+2] / z2) * bc) * z;

            vec3 albedo = mesh.texture[uvec2(texCoord * mesh.texture.size)] / 255.0f;

            vec3 color = albedo;

            // struct Light {
            //     vec3 position;
            //     vec3 color;
            // } lights[] {
            //     { .position = vec3(-10, 0, 0), .color = vec3(1, 0, 0) },
            //     { .position = vec3(+10, 0, 0), .color = vec3(0, 1, 0) },
            //     { .position = vec3(0, +10, 0), .color = vec3(0, 0, 1) }
            // };

            // vec3 radiance = 0;

            // for (Light light : lights) {
            //     vec3 lightPosition = vec3(viewMat * vec4(light.position, 1));
            //     vec3 lightVector = normalize(lightPosition - position);
            //     float diffuse = max(dot(normal, lightVector), 0);

            //     vec3 reflectedLightVector = normalize(reflect(-lightVector, normal));
            //     vec3 viewVector = normalize(vec3(0) - position);
            //     float specular = pow(max(dot(reflectedLightVector, viewVector), 0), 100);

            //     radiance += light.color * (diffuse * 0.5 + specular * 0.5);
            // }

            // color *= radiance;

            // setRandomState(i);
            // color *= getRandom<vec3>();

            colorBuffer[p] = clamp(color, 0, 1) * 255;
        }
    }
}

mat4 getViewMat() {
    vec2 viewAngleDelta = getMouseDelta();

    static vec2 viewAngles = vec2(0);
    viewAngles += viewAngleDelta * 0.002;        

    mat4 viewRotationMat = rotationMatY(viewAngles.x) * rotationMatX(viewAngles.y);

    vec3 viewPositionDelta(
        isKeyPressed(keys::D) - isKeyPressed(keys::A),
        isKeyPressed(keys::SPACE) - isKeyPressed(keys::LEFT_SHIFT),
        isKeyPressed(keys::W) - isKeyPressed(keys::S)
    );
    if (length(viewPositionDelta)) viewPositionDelta = normalize(viewPositionDelta);
    viewPositionDelta = vec3(viewRotationMat * vec4(viewPositionDelta, 0));

    static vec3 viewPosition = vec3(0, 0, -2.5);
    viewPosition += viewPositionDelta * getFrameDelta() * 3;

    static vec3 smoothViewPosition = viewPosition;
    smoothViewPosition += (viewPosition - smoothViewPosition) * min(getFrameDelta() * 10, 1);

    mat4 viewTranslationMat = translationMat(smoothViewPosition); 

    return inverse(viewTranslationMat * viewRotationMat);
}

int main(int argc, char** argv) {
    Array2<u8vec3> windowBuffer(uvec2(1000));
    Array2<u8vec3> colorBuffer(uvec2(1000));
    Array2<float> depthBuffer(colorBuffer.size);

    // Mesh mesh = readOBJ("spot.obj");

    Mesh mesh;

    mesh.texture = Array2<u8vec3>(uvec2(20));
    for (ivec2 p : range(mesh.texture.size)) {
        mesh.texture[p] = (p.x % 2) ^ (p.y % 2) ? 255 : 0;
    }

    mesh.texCoords.append(vec2(0, 0));
    mesh.texCoords.append(vec2(1, 0));
    mesh.texCoords.append(vec2(1, 1));

    mesh.texCoords.append(vec2(0, 0));
    mesh.texCoords.append(vec2(1, 1));
    mesh.texCoords.append(vec2(0, 1));

    for (vec2 texCoord : mesh.texCoords) {
        mesh.vertices.append(vec3(texCoord * 2 - 1, 0));
        mesh.normals.append(vec3(0, 0, 1));
    }

    initClock();
    initWindow("rasterizer", windowBuffer.size);
    setVsyncEnabled(false);
    setCursorEnabled(false);

    while (!windowShouldClose()) {
        colorBuffer.fill(0.1 * 255);
        depthBuffer.fill(1);

        // mat4 objectMat = translationMat(vec3(0, 0, 0)) * rotationMatY(PI + getTime());
        // mat4 viewMat = translationMat(vec3(0, 0, 2.5));
        // mat4 projectionMat = perspectiveMat(toRadian(60), 0.1, 10);

        mat4 objectMat = translationMat(vec3(0, 0, 0)) * rotationMatY(PI);
        mat4 viewMat = getViewMat();
        mat4 projectionMat = perspectiveMat(toRadian(90), 0.1, 10);

        rasterizeTriangles(colorBuffer, depthBuffer, mesh, objectMat, viewMat, projectionMat);

        for (uvec2 p : range(windowBuffer.size))
            windowBuffer[p] = colorBuffer[p * (colorBuffer.size-1) / (windowBuffer.size-1)];
            // windowBuffer[p] = depthBuffer[p * (depthBuffer.size-1) / (windowBuffer.size-1)] * 255;

        updateWindow(windowBuffer.data);

        // std::cout << 1 / getFrameDelta() << " fps" << std::endl;
    }
}
